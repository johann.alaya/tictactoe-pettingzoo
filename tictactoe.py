import os
import random
import hashlib
import numpy as np
from collections import defaultdict
import dill
from pettingzoo.classic import tictactoe_v3

#politique: choix des actions au hasard
def random_policy(observation,agent):
    action = random.choice(np.flatnonzero(observation['action_mask']))
    return action
    
#fonction d'encodage
def encode_state(observation):
    # encode observation as bytes           
    obs_bytes = str(observation).encode('utf-8')
    # create md5 hash
    m = hashlib.md5(obs_bytes)
    # return hash as hex digest
    state = m.hexdigest()
    return(state)

def exercice1(): #On réalise n parties au hasard et on analyse les résultats.
    n=None
    while (type(n)!=int):
        print("Nombre de parties : ")
        n=int(input())
    #créer l'environnement
    env = tictactoe_v3.env()

    p2wins=0
    draws=0
    for i in range(n):
        env.reset()
        for agent in env.agent_iter():
            observation, reward, done, info = env.last()
            action=random_policy(observation,agent) if not done else None
            env.step(action)
            #env.render()
            if done:
                #Si la récompense est nulle, on sait directement que la partie est nulle
                if(reward==0):
                    draws+=1
                #L'agent qui a perdu est le premier a etre appelé pour recevoir sa récompense
                # (car l'agent gagnant est celui qui fait une action différent de None en dernier)
                #donc on sait que c'est l'agent courant qui a perdu
                elif(agent=='player_1'):
                    p2wins+=1
                env.step(None)
    #pas besoin de comptabiliser les victoires des deux agents car on peut le déduire a la fin
    p1wins = n - draws - p2wins
    print("Après avoir joué ", n, " parties au hasard, le joueur 1 a gagné ", ((p1wins/n)*100), "% du temps, le joueur 2 a gagné ", ((p2wins/n)*100), "% du temps et la partie s'est conclue par une égalité ", ((draws/n)*100), "% du temps.", sep='')        
    
def exercice2():
    n=None
    while (type(n)!=int):
        print("Nombre de parties : ")
        n=int(input())

    #créer l'environnement + defaultdict
    env = tictactoe_v3.env()
    Q = defaultdict(lambda: np.zeros(9)) #9 actions possibles au maximum dans chaque état

    for i in range(n):
        env.reset()
        for agent in env.agent_iter():
            observation, reward, done, info = env.last()
            state = encode_state(env.render(mode = 'ansi'))
            Q[state]
            action=random_policy(observation,agent) if not done else None
            env.step(action)
    print(len(Q))
    #print(Q)

#Exercice3
prev_state = {'player_1': -1,
              'player_2': -1}
prev_action = {'player_1': -1,
              'player_2': -1}

def epsilon_greedy_policy(nA, Q, agent, action_mask, state, eps):
    a=random.randrange(0, 100)
    if(a > eps * 100.0):
        b=[(Q[agent][state][i],i) for i in range(len(action_mask)) if action_mask[i]==1]
        c=np.argmax([b[i][0] for i in range(len(b))])
        return b[c][1] #C'est l'indice de l'argmax dans l'ancien tableau, car les indices dans b ne sont pas les
        #mêmes... c'est d'ailleurs pour ça qu'on stocke des tuples et pas des simples valeurs
    else:
        return random.choice([i for i in range(len(action_mask)) if action_mask[i]==1])

def update_Q_value(Q, agent, previous_state, previous_action, reward, alpha, gamma, current_state = None):
    if(previous_state!=-1 and previous_action!=-1):
        Q[agent][previous_state][previous_action] = Q[agent][previous_state][previous_action] + alpha * (reward + gamma * np.max(Q[agent][current_state][:]) - Q[agent][previous_state][previous_action])
    return Q

def marl_q_learning(multi_env, num_episodes, alpha, gamma=1.0, eps_start=0.2, eps_decay=.99999, eps_min=0.025):
    
    current = os.getcwd()
    multi_env.reset()
    Q = {}
    for agent in multi_env.agents:    
        nA = multi_env.action_spaces[agent].n
        Q[agent] = defaultdict(lambda: np.zeros(nA))

    epsilon = eps_start

    i_episode = 1    

    prev_state = {'player_1': -1,
                  'player_2': -1}
    prev_action = {'player_1': -1,
                  'player_2': -1}

    # keeps iterating over active agents until num episode break
    while i_episode <= num_episodes:
        for agent in multi_env.agent_iter():        
            
            # get observation (state) for current agent:
            observation, reward, done, info = multi_env.last()
            
            # perform q-learning with update_Q_value()
            state = encode_state(multi_env.render(mode = 'ansi'))
            Q = update_Q_value(Q, agent, prev_state[agent], prev_action[agent], reward, alpha, gamma, state)
            
            # store current state
            prev_state[agent] = state
            
            if not done: 
                # choose action using epsilon_greedy_policy()
                action = epsilon_greedy_policy(nA, Q, agent, observation['action_mask'], state, epsilon)       
                multi_env.step(action)
                
                # store chosen action
                prev_action[agent] = action 
            else: 
                # agent is done
                multi_env.step(None)
                        
        # reset env and memory
        multi_env.reset()
        prev_state = {'player_1': -1,
                      'player_2': -1}
        prev_action = {'player_1': -1,
                       'player_2': -1}
        # bump episode
        i_episode += 1
        if (i_episode % 10000==0):
            print(i_episode)
            if (i_episode % 50000==0):
                with open(current + '\Qvingt' + str(i_episode) + '.pkl', 'wb') as f:
                    dill.dump(Q, f)
        # decrease epsilon
        epsilon = max(epsilon*eps_decay, eps_min)
    return Q   

def confront_players(multi_env, n, Q_player1, Q_player2=None):
    Q={'player_1':Q_player1,'player_2':Q_player2}
    multi_env.reset()
    for agent in multi_env.agents:
        nA = multi_env.action_spaces[agent].n
    p2wins = 0
    draws = 0
    for i in range(n):
        for agent in env.agent_iter():
            state = encode_state(multi_env.render(mode = 'ansi'))
            observation, reward, done, info = env.last()
            if not done:
                if(agent=='player_1'):
                    if(Q_player1!=None):
                        action=epsilon_greedy_policy(nA, Q, agent, observation['action_mask'], state, 0.0)
                    else:
                        action=random_policy(observation, agent)
                elif(Q_player2!=None):
                    action=epsilon_greedy_policy(nA, Q, agent, observation['action_mask'], state, 0.0)
                else:
                    action=random_policy(observation, agent)
                multi_env.step(action)
            else:
                if(reward==0):
                    draws+=1
                elif(agent=='player_1'):
                    p2wins+=1
                multi_env.step(None)
                multi_env.step(None) #Il faut faire deux steps car on commence à l'état vide
        multi_env.reset()
    p1wins = n - p2wins - draws
    return [p1wins, p2wins, draws]

if __name__ == "__main__":
    #exercice1()
    #exercice2()
    env = tictactoe_v3.env()
    random.seed(123)
    current = os.getcwd()
    a = ""
    while(a!="y" and a!="Y" and a!="n" and a!="N"):
        a=input("Relancer l'entrainement ? (Y/N, si N, tente d'utiliser une Q-Table déjà générée.) : ")
    if(a=="y" or a=="Y"):
        n=None
        while (type(n)!=int):
            print("Nombre d'épisodes pour l'entrainement : ")
            n=int(input())
        Q = marl_q_learning(env, n, alpha = 0.6, gamma = 0.95)
        with open(current + '\Qvingttest.pkl', 'wb') as f:
            dill.dump(Q, f)
    else:
        a=input("Entrer le nom du fichier contenant la table a tester (sans l'extension) : ")
        with open(current + '\\' + a + '.pkl', 'rb') as f:
            Q = dill.load(f)
        #Test vs adversaire aléatoire pour j1
        [p1wins_1, p2wins_1, draws_1] = confront_players(env, 10000, Q['player_1'])
        print("Confrontation 1 : done")
        #Test vs adversaire aléatoire pour j2
        [p1wins_2, p2wins_2, draws_2] = confront_players(env, 10000, None, Q['player_2'])
        print("Confrontation 2 : done")
        #Test entre les 2 IA
        [p1wins_3, p2wins_3, draws_3] = confront_players(env, 10000, Q['player_1'], Q['player_2'])
        print("Confrontation 3 : done")
        print("Résultats IA j1 vs aléatoire j2 : victoires : " + str(p1wins_1) + " défaites : " + str(p2wins_1) + " égalités : " + str(draws_1))
        print("Résultats IA j2 vs aléatoire j1 : victoires : " + str(p2wins_2) + " défaites : " + str(p1wins_2) + " égalités : " + str(draws_2)) 
        print("Résultats IA j1 vs IA j2 : victoires j1 : " + str(p1wins_3) + " victoires j2 : " + str(p2wins_3) + " égalités : " + str(draws_3))